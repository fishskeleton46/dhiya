<?php

namespace Croogo\Trainings\Model\Table;

use Cake\Database\Schema\TableSchema;
use Cake\Event\Event;
use Cake\ORM\Entity;
use Cake\Validation\Validator;
use Croogo\Core\Model\Table\CroogoTable;

/**
 * Link
 *
 * @category Model
 * @package  Croogo.Trainings.Model
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class TrainingSchedules extends CroogoTable
{

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notBlank('title', __d('croogo', 'Title cannot be empty.'));

        $validator
            ->add('link', 'custom', [
                'rule' => function($value, $context) {
                    return !empty($value);
                },
                'message' => __d('croogo', 'Link cannot be empty.')
            ]);

        return $validator;
    }

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Croogo/Core.Cached', [
            'groups' => ['trainings']
        ]);
        $this->belongsTo('Trainings', [
            'className' => 'Croogo/Trainings.Trainings',
        ]);

        $this->addBehavior('Timestamp', [
            'events' => [
                'Model.beforeSave' => [
                    'created' => 'new',
                    'updated' => 'always',
                ],
            ],
        ]);

        $this->addBehavior('Croogo/Core.Publishable');
        $this->addBehavior('Croogo/Core.Visibility');
        $this->addBehavior('Search.Search');

        $this->searchManager()
            ->add('menu_id', 'Search.Value', [
                'field' => 'menu_id'
            ]);
    }

    protected function _initializeSchema(TableSchema $table)
    {
        $table->columnType('visibility_roles', 'encoded');

        return parent::_initializeSchema($table);
    }

    /**
     * Calls TreeBehavior::recover when we are changing scope
     */
    public function afterSave(Event $event, Entity $entity, $options = [])
    {
        
    }
}
