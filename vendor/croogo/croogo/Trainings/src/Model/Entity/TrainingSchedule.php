<?php

namespace Croogo\Trainings\Model\Entity;

use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;
use Croogo\Acl\Traits\RowLevelAclTrait;

class TrainingSchedule extends Entity
{

    use RowLevelAclTrait;

    use TranslateTrait;

}
