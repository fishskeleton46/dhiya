<?php

namespace Croogo\Trainings\Config;

use Croogo\Core\Nav;

Nav::add('sidebar', 'Trainings', [
    'icon' => 'sitemap',
    'title' => __d('croogo', 'Trainings'),
    'url' => [
        'prefix' => 'admin',
        'plugin' => 'Croogo/Trainings',
        'controller' => 'Trainings',
        'action' => 'index',
    ],
    'weight' => 20,
    'children' => [
        'Trainings' => [
            'title' => __d('croogo', 'Trainings'),
            'url' => [
                'prefix' => 'admin',
                'plugin' => 'Croogo/Trainings',
                'controller' => 'Trainings',
                'action' => 'index',
            ],
            'weight' => 10,
        ],
    ],
]);
