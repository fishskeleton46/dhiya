<?php

use Cake\Core\Configure;
use Cake\Cache\Cache;
use Croogo\Core\Croogo;

Cache::config('croogo_trainings', array_merge(
    Configure::read('Croogo.Cache.defaultConfig'),
    ['groups' => ['trainings']]
));