<?php

namespace Croogo\Settings\Model\Table;

use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Validation\Validator;
use Croogo\Core\Model\Table\CroogoTable;

use Cake\Utility\Text;
use Cake\Core\Configure;
use Cake\Datasource\EntityInterface;

/**
 * Language
 *
 * @category Model
 * @package  Croogo.Settings.Model
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
class LanguagesTable extends CroogoTable
{
    public $uploadsDir = 'uploads';

/**
 * Initialize
 */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('Croogo/Core.Trackable');
        $this->addBehavior('ADmad/Sequence.Sequence', [
            'order' => 'weight',
        ]);
        $this->addBehavior('Search.Search');
        $this->addBehavior('Timestamp');

        $likeOptions = [
            'before' => true,
            'after' => true,
        ];
        $this->searchManager()
            ->add('title', 'Search.Like', $likeOptions)
            ->add('alias', 'Search.Like', $likeOptions)
            ->add('locale', 'Search.Like', $likeOptions);
    }

    public function validationDefault(Validator $validator)
    {
        $validator
            ->notBlank('title', __d('croogo', 'Title cannot be empty.'))
            ->notBlank('native', __d('croogo', 'Native cannot be empty.'))
            ->notBlank('alias', __d('croogo', 'Alias cannot be empty.'))
            ->notBlank('locale', __d('croogo', 'Locale cannot be empty.'));
        return $validator;
    }

    public function buildRules(RulesChecker $rules)
    {
        $rules
            ->add($rules->isUnique(['locale'],
                __d('croogo', 'That locale is already taken')
            ))
            ->add($rules->isUnique( ['alias'],
                __d('croogo', 'That alias is already taken')
            ));
        return $rules;
    }

    public function findActive(Query $query)
    {
        $query
            ->select(['id', 'alias', 'locale'])
            ->where(['status' => true])
            ->formatResults(function ($results) {
                $formatted = [];
                foreach ($results as $row) {
                    $formatted[$row->alias] = ['locale' => $row->locale];
                }
                return $formatted;
            });
        return $query;
    }


    /**
 * Save uploaded file
 *
 * @param array $entity data as POSTed from form
 * @return array|boolean false for errors or array containing fields to save
 */
    protected function _saveUploadedFile($entity)
    {
        $file = $entity->file;
        $dir = WWW_ROOT . $this->uploadsDir . DS . 'flags';

        // Check if dir exists
        if (!file_exists($dir)) {

            // Check if debug is enabled, to be consistent on only creating
            // folders when debug is enabled across the whole framework.
            if (Configure::read('debug')) {
                mkdir($dir);
            } else {
                return false;
            }
        }

        // check if file with same path exists
        $destination = $dir . DS . $file['name'];
        if (file_exists($destination)) {
            $newFileName = Text::uuid() . '-' . $file['name'];
            $destination = $dir . DS . $newFileName;
        } else {
            $newFileName = $file['name'];
        }

        // remove the extension for title
        if (explode('.', $file['name']) > 0) {
            $fileTitleE = explode('.', $file['name']);
            array_pop($fileTitleE);
            $fileTitle = implode('.', $fileTitleE);
        } else {
            $fileTitle = $file['name'];
        }

        $entity->mime_type = $file['type'];
        $entity->language_flag = '/' . $this->uploadsDir . '/flags/' . $newFileName;
        // move the file
        $moved = move_uploaded_file($file['tmp_name'], $destination);
        if ($moved) {
            return $entity;
        }

        return false;
    }

    /**
 * Saves model data
 *
 * @see Model::save()
 */
    public function save(EntityInterface $entity, $options = [])
    {
        $data = true;
        if (isset($entity->file['tmp_name'])) {
            $data = $this->_saveUploadedFile($entity);
        }
        if (!$data) {
            return $entity->errors(['file' => __d('croogo', 'Error during file upload')]);
        }
        return parent::save($entity, $options);
    }
}
