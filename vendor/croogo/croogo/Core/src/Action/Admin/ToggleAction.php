<?php

namespace Croogo\Core\Action\Admin;

use Crud\Action\BaseAction;
use Exception;

class ToggleAction extends BaseAction
{
    protected $_defaultConfig = [
        'enabled' => true,
        'field' => 'status'
    ];

    /**
     * Toggle Link status
     *
     * @param int $id Link id
     * @param int $status Current Link status
     * @throws Exception
     * @return void
     */
    protected function _post($id = null, $status = null, $field = null)
    {
        if (empty($id) || $status === null) {
            throw new Exception(__d('croogo', 'Invalid content'));
        }

        $status = (int)!$status;

        /* custom by iksan */
        $field = !empty($field) ? $field : $this->config('field');
        /* custom by iksan */

        $this->_controller()->viewBuilder()->setLayout('ajax');
        $this->_controller()->viewBuilder()->template('Croogo/Core./Common/admin_toggle');

        $entity = $this->_table()->get($id);
        $entity->set($field, $status);
        if (!$this->_table()->save($entity)) {
            throw new Exception(__d('croogo', 'Failed toggling field %s to %s', $field, $status));
        }

        $this->_controller()->set(compact('id', 'status'));
    }
}
